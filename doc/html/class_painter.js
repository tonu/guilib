var class_painter =
[
    [ "drawLine", "class_painter.html#a8ab74a74b1c5da6e1ce91f5aba2374fe", null ],
    [ "drawLine", "class_painter.html#a71d4484923e8d16e271ad8cdec460833", null ],
    [ "drawText", "class_painter.html#abab2ce780d7106e95b300e960f4bc973", null ],
    [ "drawTriangle", "class_painter.html#ac650ced08730b037ab7e3f40aba810a1", null ],
    [ "fillRect", "class_painter.html#ab0eed870565aa9040d21a3e62824e984", null ],
    [ "flush", "class_painter.html#afd4a16abb9ab2f44870e8b497d7921c6", null ],
    [ "setClipRect", "class_painter.html#ace59aebb753297a01c64f166bdc2165d", null ],
    [ "setColor", "class_painter.html#a7992ad23f02bed3c98b3edabb9e803c3", null ],
    [ "setOrigin", "class_painter.html#ac74e94cab245e13274340c1d041272ae", null ],
    [ "_b", "class_painter.html#a2d3221fd455b3df70c635600936138ad", null ],
    [ "_g", "class_painter.html#ae273e01cd199e13b9983599bc001aff3", null ],
    [ "_origin", "class_painter.html#aab51b8b2890c4c333e378375a9322aeb", null ],
    [ "_r", "class_painter.html#ac247e68a4873db39e62abe6d4e1b610a", null ]
];