var class_event =
[
    [ "Type", "class_event.html#a2abf13b5be49315e9e362af02029f058", [
      [ "KeyPress", "class_event.html#a2abf13b5be49315e9e362af02029f058ab4514b278f5e3b059ae4e4328bc24512", null ],
      [ "KeyRelease", "class_event.html#a2abf13b5be49315e9e362af02029f058a561bf14bfddc6a8e3bd3f20e045fee90", null ],
      [ "MousePress", "class_event.html#a2abf13b5be49315e9e362af02029f058a4b2adb39cb2de79c8b8659bd47f9c9e1", null ],
      [ "MouseRelease", "class_event.html#a2abf13b5be49315e9e362af02029f058ad5b57527f9cf7e8a791ba5fc39b06582", null ],
      [ "FocusIn", "class_event.html#a2abf13b5be49315e9e362af02029f058ad6911917ad9882991b7570328261b6ef", null ],
      [ "FocusOut", "class_event.html#a2abf13b5be49315e9e362af02029f058aec540cdb5564af475b47dee20b109d09", null ],
      [ "Resize", "class_event.html#a2abf13b5be49315e9e362af02029f058a5e7a713b38a460fff348986e1727a708", null ],
      [ "Layout", "class_event.html#a2abf13b5be49315e9e362af02029f058aeb9615d9a9077c9c7f6ad4792f72d68a", null ],
      [ "Timer", "class_event.html#a2abf13b5be49315e9e362af02029f058a22110d3fe1880985d54ce4414b5e62c2", null ],
      [ "Paint", "class_event.html#a2abf13b5be49315e9e362af02029f058a569a2fe221001907df438e6993910d54", null ],
      [ "Invoke", "class_event.html#a2abf13b5be49315e9e362af02029f058a10b15c4542a1f1fe9be344e41a2fdc05", null ]
    ] ],
    [ "Event", "class_event.html#a7f0f94771d5a4c8fc59a6aa564e6eb3b", null ],
    [ "~Event", "class_event.html#a7704ec01ce91e673885792054214b3d2", null ],
    [ "data", "class_event.html#a3e8d45074315ea934d34c914b13a844e", null ],
    [ "receiver", "class_event.html#afe6afe85409829b0a93a31a3ed8bdb41", null ],
    [ "type", "class_event.html#acb1e67cf0d5f22ac9360447b6f0fb5d3", null ]
];