var class_key_event_data =
[
    [ "KeyCodeFocusNext", "class_key_event_data.html#a0b9a0c1478469460abcd9df8fa5c3252a65d9cc8d139f0bef5609fb9d067d700f", null ],
    [ "KeyCodeFocusPrev", "class_key_event_data.html#a0b9a0c1478469460abcd9df8fa5c3252a6e205cbd138ee973b082b288aa65ef25", null ],
    [ "KeyCodeBackspace", "class_key_event_data.html#a0b9a0c1478469460abcd9df8fa5c3252a457350155a7086a69c4daa2bf89dda47", null ],
    [ "KeyCodeEnter", "class_key_event_data.html#a0b9a0c1478469460abcd9df8fa5c3252aa5ca11a604cae99e907737622cc1a3fb", null ],
    [ "KeyEventData", "class_key_event_data.html#a082aef712b07912db92b7f4b81cbda0d", null ],
    [ "_code", "class_key_event_data.html#aaf5a60f49e581d302ceb6bcee073e874", null ],
    [ "_pressed", "class_key_event_data.html#a3241a27cef152b71eb993158d4464343", null ]
];