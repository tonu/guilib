var hierarchy =
[
    [ "Application", "class_application.html", [
      [ "App", "class_app.html", null ],
      [ "AppUCOS", "class_app_u_c_o_s.html", null ]
    ] ],
    [ "Binding", "class_binding.html", null ],
    [ "color", "structcolor.html", null ],
    [ "Color", "class_color.html", null ],
    [ "List< T >::ConstIterator", "class_list_1_1_const_iterator.html", null ],
    [ "Display", "class_display.html", [
      [ "GLDisplay", "class_g_l_display.html", null ]
    ] ],
    [ "Event", "class_event.html", null ],
    [ "Focus", "class_focus.html", null ],
    [ "List< T >::Iterator", "class_list_1_1_iterator.html", null ],
    [ "KeyEventData", "class_key_event_data.html", null ],
    [ "Layout", "class_layout.html", null ],
    [ "List< T >", "class_list.html", null ],
    [ "List< Binding >", "class_list.html", null ],
    [ "List< char >", "class_list.html", [
      [ "Str", "class_str.html", null ]
    ] ],
    [ "List< INT32 >", "class_list.html", [
      [ "Vec", "class_vec.html", null ]
    ] ],
    [ "List< Wi * >", "class_list.html", null ],
    [ "Matrix< COLS, ROWS >", "class_matrix.html", null ],
    [ "Matrix< 1, 4 >", "class_matrix.html", null ],
    [ "Matrix< 4, 4 >", "class_matrix.html", null ],
    [ "Obj", "class_obj.html", [
      [ "Wi", "class_wi.html", [
        [ "GLWi", "class_g_l_wi.html", null ],
        [ "ListWi", "class_list_wi.html", null ]
      ] ]
    ] ],
    [ "Painter", "class_painter.html", [
      [ "PainterMono", "class_painter_mono.html", null ],
      [ "PainterQt", "class_painter_qt.html", null ]
    ] ],
    [ "Point", "class_point.html", [
      [ "Point3", "class_point3.html", null ]
    ] ],
    [ "point3", "structpoint3.html", null ],
    [ "QGLWidget", null, [
      [ "Widget", "class_widget.html", null ]
    ] ],
    [ "Rect", "class_rect.html", null ],
    [ "ResizeEventData", "class_resize_event_data.html", null ],
    [ "RingBuf< T >", "class_ring_buf.html", null ],
    [ "RingBuf< Event * >", "class_ring_buf.html", null ],
    [ "Slo", "class_slo.html", [
      [ "Slot< T >", "class_slot.html", null ]
    ] ]
];