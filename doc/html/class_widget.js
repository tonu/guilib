var class_widget =
[
    [ "Widget", "class_widget.html#a29531c7f141e461322981b3b579d4590", null ],
    [ "initializeGL", "class_widget.html#ad686f34cc3a9a553f47bc26c98db7099", null ],
    [ "keyPressEvent", "class_widget.html#a8b3a6948c3fe5e6e2c2f6c0920e66086", null ],
    [ "keyReleaseEvent", "class_widget.html#a67380b178fcbd58770815eb3e5c892ca", null ],
    [ "paintGL", "class_widget.html#a401b37706e91ef11c214fc11e5aad814", null ],
    [ "resizeEvent", "class_widget.html#a6cc0d94bddf7097ace5f0a246c4bc17e", null ],
    [ "resizeGL", "class_widget.html#a5a7a616da975168c7fd5bb0a777615cc", null ],
    [ "timerEvent", "class_widget.html#aceb75d32b8dd4e94655246ca8fd26093", null ],
    [ "update", "class_widget.html#a941db1e44788465c0ef5f49be527a3a8", null ]
];