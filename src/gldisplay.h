#ifndef GLDISPLAY_H
#define GLDISPLAY_H

#include "display.h"

class GLDisplay : public Display
{
public:
    void blit(const unsigned char *data);
};

#endif // GLDISPLAY_H
