#include "glwi.h"
#include "painter.h"
#include "testmodel.h"

#include "sinlut.h"
#include "matrix.h"
#include "util.h"


 //m.perspective(45, _rect._w / (qreal)_rect._h, 1.0 ,500);
INT16S projMat[16] = {
    0.906282*256,     0,         0,         0,
           0,   2.41421*256,     0,         0,
           0,         0,  -1.00401*256,    -2.00401*256,
           0,         0,        -1*256,     0,          };

GLWi::GLWi(Wi *parent, Layout *layout)
    :
    Wi(parent, layout)
{
    model.loadIdent();

    proj.load(projMat);


}

void GLWi::transform(const point3 IAR_FLASH &vert, Point3 &out)
{
    vertIn._data[0] = vert.x;
    vertIn._data[1] = vert.y;
    vertIn._data[2] = vert.z;
    vertIn._data[3] = 256;

    projAndModel.mult(vertIn, vertOut);

    if (vertOut._data[3] == 0)
        vertOut._data[3] = 1; // could crash otherwise

    out._x = (vertOut._data[0]*(_rect._w>>1)) / vertOut._data[3] +  (_rect._w>>1);
    out._y = (vertOut._data[1]*(_rect._h>>1)) / vertOut._data[3] +  (_rect._h>>1);
    out._z = vertOut._data[2]<<8 / vertOut._data[3];
}

bool GLWi::isClockwise(const Point3 &p1, const Point3 &p2, const Point3 &p3)
{
    return 0 > ((INT16S)p2._x - p1._x) * ((INT16S)p3._y - p1._y) - ((INT16S)p2._y - p1._y) * ((INT16S)p3._x - p1._x);
}

void GLWi::paintEvent()
{
    static INT8S t=0; t++;

    if (_hasFocus) {
        painter->setColor(0,0,255);

        painter->drawLine(0, 0, _rect._w, 0);
        painter->drawLine(0, 0, 0, _rect._h);
        painter->drawLine(_rect._w, _rect._h, _rect._w, 0);
        painter->drawLine(_rect._w, _rect._h,  0, _rect._h);
    }

    model.loadIdent();
    model.translate(t<<3,0,-8.6 * 256);

    model.rotate(-100,t,0);
    //model.scale(512,512,512);
   //model.rotate(0,t*1,0);

    proj.mult(model, projAndModel);

    painter->setColor(10,10,10);
    for (INT16S x=0;x<numGourdFaces;x++)
    {
        int IAR_FLASH *i = GourdFaces[x];

        Point3 p1, p2, p3;

 //       transform(GourdVerts[*i], p1,p2,p3); i+=3;
        transform(GourdVerts[*i++], p1);
        transform(GourdVerts[*i++], p2);
        transform(GourdVerts[*i++], p3);

        // find, is the triangle clockwise or not.if not then dont draw as we dont have depth buf
        if (isClockwise(p1,p2,p3)) // could do this check before perspective division to save time
        {
            painter->drawTriangle(p1, p2, p3);
        }
    }

    Str s("f:...");
    s[2] = 0x30 + (INT8U)t/100;
    s[3] = 0x30 + ((INT8U)t/10) % 10;
    s[4] = 0x30 + ((INT8U)t) % 10;
    painter->drawText(2,2,s);
   // painter->drawText(2,22,"hold down a key");
}

void GLWi::keyPressEvent(KeyEventData *dat)
{
    update();
}

void GLWi::keyReleaseEvent(KeyEventData *dat)
{

}
