#include "application.h"
#include "wi.h"
//#include <ucos_ii.h> // only for types

Application *app = 0;

Application::Application()
{
    app = this;
    _painter = 0;// whoever subclasses, must set it
}

void Application::execute()
{

    while (1)
    {
        Event *ev = pendEvent();

        if (ev == NULL) // a safety feature
            continue;

        // some events dont have a target receiver while they are posted, (like keypress
        if (ev->receiver == NULL)
        {
            if (ev->type == Event::KeyPress || ev->type == Event::KeyRelease)
            { // do some routing. keypress events without specific receiver are routed to keyboardGrabber

                Wi *_grabber = Wi::_keyboardGrabberWidget;

                if (_grabber != NULL)
                    ev->receiver = _grabber;  // send the keypress
            }
        }

        // in case we have found a reveiver by now, we call the handler
        if (ev->receiver != NULL)
            ev->receiver->handleEvent(ev);

        // by now events created, should be "used", so we can free
        delete ev;
    }
}

