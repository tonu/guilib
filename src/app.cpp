#include "app.h"
#include "event.h"
#include "wi.h"
#include "listwi.h"
#include "paintermono.h"
#include "glwi.h"

App::App()
    :
    Application(),
    eventList(100) // if its filled full by itself (thread), it will halt forever
{
    Wi * _topWidget = new Wi(0, new Layout());

    Focus *foc = new Focus;

    Wi *w1 = new GLWi(_topWidget);
    Wi *w2 = new Wi(_topWidget, new Layout(Layout::Horizontal));

    Wi *w21 = new ListWi(w2);
    ListWi *w22 = new ListWi(w2);

    w1->setFocus(foc);
    w21->setFocus(foc);
    w22->setFocus(foc);

    w1->grabFocus();

    _topWidget->setRect(Rect(0,0, 160*4, 128*4));
    _topWidget->doLayout();

    w2->doLayout();

    w21->connect("test", ISLOT(w22, ListWi, setText));

}

App::~App()
{

}

void App::post(Event *event)
{
    eventList.put(event);
}

Event * App::pendEvent()
{
    return eventList.get();;
}

void App::run()
{
    execute();
}

Painter *App::painter()
{
    if (_painter == 0)
        _painter = new PainterMono;

    return _painter;
}
