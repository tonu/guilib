#ifndef EVENT_H
#define EVENT_H

#include "util.h"

class Obj;

class ResizeEventData
{
public:
    ResizeEventData( const Rect &oldRect, const Rect &newRect)
        :
        _oldRect(oldRect),
        _newRect(newRect)
    {
    }

    Rect _oldRect;
    Rect _newRect;
};

class KeyEventData
{
public:

#if 1
    enum {
        KeyCodeFocusNext = 103| 0x80, // if 7th bit is up, special key
        KeyCodeFocusPrev = 101| 0x80,
        KeyCodeBackspace = 8,
        KeyCodeEnter = 13,
    };
#else
    enum {
        KeyCodeFocusNext = 21,
        KeyCodeFocusPrev = 19
    };
#endif
    KeyEventData(INT16U code, bool pressed)
        :
        _code(code),
        _pressed(pressed)
    {
    }

    INT16U _code;
    bool _pressed;
};

class Event
{
public:

    enum Type {
        KeyPress, //0
        KeyRelease,
        MousePress,
        MouseRelease,
        FocusIn,      // will be sent
        FocusOut,
        Resize,       // widget was resized already
        Layout, //7
        Timer,
        Paint, // 9
        Invoke
    };

    Event(Type type, Obj *receiver, void *evData);
    ~Event();

    Type type;
    Obj *receiver;
    void *data;
};

#endif // EVENT_H
