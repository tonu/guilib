#ifndef WIDGET_H
#define WIDGET_H

#include "app.h"

#include <QGLWidget>

class QEvent;
class QKeyEvent;
class QPaintEvent;
class QResizeEvent;

class Widget;
extern Widget * gWidget;

namespace Ui {
    class Widget;
}

class Widget : public QGLWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);

    void update(const unsigned char *data);

public slots:

protected:
    void keyPressEvent ( QKeyEvent * event );
    void keyReleaseEvent ( QKeyEvent * event );

    void resizeEvent ( QResizeEvent * event );
    void timerEvent(QTimerEvent *event);

    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

private:

    App *_app;
    const unsigned char *_data;
};

#endif // WIDGET_H
