#ifndef LISTWI_H
#define LISTWI_H

#include "util.h"
#include "wi.h"

class ListWi : public Wi
{
public:
    ListWi(Wi *parent = 0, Layout *layout = 0);

    virtual void paintEvent();

    virtual void keyPressEvent(KeyEventData *dat);
    virtual void keyReleaseEvent(KeyEventData *dat);

    void setText(const Str &str);

private:
    Str _text;
};

#endif // LISTWI_H
