#ifndef MATRIX_H
#define MATRIX_H

#include "util.h"

template <INT8U COLS, INT8U ROWS>
class Matrix
{
public:

    void loadIdent();
    void load(const Matrix<COLS,ROWS> & src);
    void load(const INT16S * data);


    void mult(const Matrix<4,4> &right, Matrix<4,4> & output) const;
    void mult(const Matrix<1,4> &right, Matrix<1,4> & output) const;

    void mult(const Matrix &right);

    void scale( INT16S xScale, INT16S yScale, INT16S zScale);
    void translate( INT16S tx, INT16S ty, INT16S tz);
    void rotate( INT16S xRot, INT16S yRot, INT16S zRot);

     INT16S _data[COLS*ROWS];

private:
};

#endif // MATRIX_H
