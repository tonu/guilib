#include "wi.h"
#include "layout.h"
#include "application.h"
#include "painter.h"

Wi *Wi::_topLevel = NULL;
Wi *Wi::_keyboardGrabberWidget = NULL;
Painter *Wi::painter = NULL;

Wi::Wi(Wi *parent, Layout *layout)
    :
    _parent(parent),
    _layout(layout),
    _foc(NULL),
    _hasFocus(false)
{
    if (painter == NULL)
        painter = app->painter();


    if (_layout)
        layout->setChildList(&_children);

    if (_parent)
        _parent->addChild(this);
    else
        _topLevel = this;
}

void Wi::paintEvent()
{

}

void Wi::render()
{
    paintEvent();

    List<Wi*>::ConstIterator it(_children.constIterator());
    while(it.isValid())
    {
        (*it)->render();
        it += 1;
    }
}

void Wi::keyPressEvent(KeyEventData *dat)
{

}

void Wi::keyReleaseEvent(KeyEventData *dat)
{

}

bool Wi::handleEvent(Event *ev)
{
    switch (ev->type)
    {
    case Event::KeyPress:
        if (ev->data)
        {
            KeyEventData * dat = (KeyEventData*)ev->data;

            if (dat->_code == KeyEventData::KeyCodeFocusNext)
            {
                focusNext();
                break;
            }
            else if (dat->_code == KeyEventData::KeyCodeFocusPrev)
            {
                focusPrev();
                break;
            }

            //qDebug() << QString("%1").arg((int)dat->_code);
            keyPressEvent(dat);
        }
        break;
    case Event::KeyRelease:
        if (ev->data)
        {
            KeyEventData * dat = (KeyEventData*)ev->data;

            if (dat->_code == KeyEventData::KeyCodeFocusNext||
                dat->_code == KeyEventData::KeyCodeFocusPrev)
            {
                break;
            }

            keyReleaseEvent(dat);
        }
        break;
    case Event::Paint:
        {
            // graphics debug
  /*          static char i = 0;
            i+=10;
            i = i & 0x7f;
*/
        //adjust painter for this widget.
        painter->setOrigin(mapToGlobal(0,0));

        painter->setClipRect(0, 0,_rect._w, _rect._h);
        painter->setColor(255,255,255);
        painter->fillRect(0, 0,_rect._w, _rect._h);
        painter->setColor(0,0,0);

        paintEvent();

        painter->flush(); //(_rect);
    }
        break;
    case Event::Resize:
        {
            update(); // schedule repaint

            //ResizeEventData * dat = (ResizeEventData*)ev->data;
            //qDebug() << "resize event:"<< dat->_oldRect << dat->_newRect;
        }
        break;
    case Event::Layout:
        if (_layout != NULL)
            _layout->exec();

        break;
    case Event::FocusOut:
        _hasFocus = false;
        update(); // schedule repaint

        break;
    case Event::FocusIn:

        if (_foc != NULL) {
            _hasFocus = true;

            update(); // repaint
        }
        break;
    default:
        return Obj::handleEvent(ev);
    }

    return true;
}


void Wi::update()
{
    if (painter != NULL)
        app->post(new Event(Event::Paint, this, 0));  // now post a paint event 'cause we need to repaint
}

void Wi::doLayout()
{
    if (_layout != NULL)
        app->post(new Event(Event::Layout, this, 0));
}

void Wi::grabFocus()
{
    if (_foc != NULL) {
        app->post(new Event(Event::FocusIn, this, 0));

        if (_keyboardGrabberWidget)
            _keyboardGrabberWidget->loseFocus();

        _keyboardGrabberWidget = this;
    }
}

void Wi::loseFocus()
{
    if (_foc != NULL && _hasFocus) {
        app->post(new Event(Event::FocusOut, this, 0));
    }
}

void Wi::addChild(Wi *child)
{
    _children.append(child);
}

void Wi::setRect(const Rect &r)
{
    Event *reszeEv = new Event(Event::Resize, this, new ResizeEventData(_rect, r)); // send old and new size

    // This func is not reentrant
    _rect = r; // change size

    app->post(reszeEv); // post resize event. it will eventually result in repaint
}

Point Wi::mapToGlobal(int x, int y)
{
    Wi *widget = this;

    Point p(widget->_rect._x, widget->_rect._y);

    while (widget->_parent)
    {
        widget = widget->_parent;
        p += Point(widget->_rect._x, widget->_rect._y);
    }

    return p;
}

void Wi::setFocus(Focus *foc)
{
    _foc = foc;

    _foc->add(this);
}

///////////////////////
// static functions
///////////////////////

void Wi::setKeyboardGrabber(Wi *widget)
{
    _keyboardGrabberWidget = widget;
}

Wi * Wi::focusPrev() // finds prev widget in chain that accepts focus and then sends event to it. returns the found widget. might return null
{
    if (_keyboardGrabberWidget == NULL)
        return NULL;

    Wi *w = _keyboardGrabberWidget->_foc->getPrev();
    w->grabFocus();

    return w;
}

Wi * Wi::focusNext() // see focusPrevious()
{
    if (_keyboardGrabberWidget == NULL)
        return NULL;

    Wi *w = _keyboardGrabberWidget->_foc->getNext();
    w->grabFocus();

    return w;
}
