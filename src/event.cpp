#include "event.h"

#include <stdlib.h>

Event::Event(Type type, Obj *receiver, void *evData)
    :
    type(type),
    receiver(receiver),
    data (evData)
{

}

Event::~Event()
{
    if (data != NULL)
        free(data);
}
