#-------------------------------------------------
#
# Project created by QtCreator 2010-08-22T13:58:52
#
#-------------------------------------------------

#QT       += core gui opengl

# g++ -enable-stdcall-fixup -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc -mthreads -Wl -Wl,-subsystem,windows -o guilib.exe object_script.guilib.Debug  -lpthread -lglut32win -lglu32 -lopengl32

TARGET = guilib
TEMPLATE = app


SOURCES += main.cpp\
    obj.cpp \
    app.cpp \
    event.cpp \
    wi.cpp \
    listwi.cpp \
    layout.cpp \
    focus.cpp \
    util.cpp \
    application.cpp \
    glwi.cpp \
    matrix.cpp \
    paintermono.cpp \
    gldisplay.cpp

HEADERS  += \
    obj.h \
    app.h \
    event.h \
    wi.h \
    listwi.h \
    layout.h \
    focus.h \
    util.h \
    painter.h \
    application.h \
    appucos.h \
    glwi.h \
    testmodel.h \
    sinlut.h \
    matrix.h \
    paintermono.h \
    font6x8.h \
    gldisplay.h \
    display.h \
    font5x7.h

FORMS    += widget.ui
INCLUDEPATH += C:/mingw/include C:/glut-3.7.6-bin
macx:LIBS +=  -lpthread -framework GLUT
win32:LIBS += -LC:/glut-3.7.6-bin -lpthread -lglut32 -lglu32 -lopengl32
