#ifndef APPLICATION_H
#define APPLICATION_H

#include "util.h"
#include "obj.h"
#include "event.h"

class Application;
class Painter;
extern Application *app;

// only instantiate one App object!
class Application
{
public:
    Application();

    virtual void post(Event *event) = 0; // event
    virtual Painter *painter() = 0;
    virtual Event * pendEvent() = 0;

    void execute(); // event loop

protected:
    Painter *_painter;
};

#endif // APPLICATION_H
