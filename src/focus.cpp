#include "focus.h"

#include "util.h"

Focus::Focus()
{
    _index = 0;
}

void Focus::add(Wi *widget)
{
    _list.append(widget);
}

Wi * Focus::getNext()
{
    _index++;
    if (_index >= _list.count())
        _index = 0;
//bug kuskil .. vajutad next next ja siin on 2kki this=l2bu
    return _list.at(_index);
}

Wi * Focus::getPrev()
{
    if (_index <= 0)
        _index =  _list.count();

    _index--;

    return _list.at(_index);
}
