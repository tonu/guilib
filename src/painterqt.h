#ifndef PAINTERQT_H
#define PAINTERQT_H

#include <QPainter>
#include "util.h"
#include "painter.h"

class Wi;
class QImage;

class PainterQt : public Painter
{
public:
    PainterQt();

    void setOrigin(const Point &p);
    void drawLine(Coord x1, Coord y1, Coord x2, Coord y2);
    void setColor(INT8U r, INT8U g, INT8U b);
    void drawText(Coord x, Coord y, const Str &str);
    void fillRect(Coord x, Coord y, Coord width, Coord height, const Color &color);
    void drawTriangle(const Point3 &p1, const Point3 &p2, const Point3 &p3);

    void flush();

private:
    QImage *screen;
    QPainter *p;
};

#endif // PAINTERQT_H
