#include "painterqt.h"
#include "wi.h"
#include "widget.h"

extern Widget *gWidget;

PainterQt::PainterQt()
{
    screen = new QImage(640,480, QImage::Format_ARGB32);
    screen->fill(0);

    p = new QPainter(screen);
    p->setRenderHint(QPainter::Antialiasing);
    p->setRenderHint(QPainter::SmoothPixmapTransform);
    _r=0;
    _g=0;
    _b=0;
}

void PainterQt::setOrigin(const Point &p)
{
    _origin = p;
}

void PainterQt::setColor(INT8U r, INT8U g, INT8U b)
{
    _r=r;
    _g=g;
    _b=b;
}

void PainterQt::drawLine(Coord x1, Coord y1, Coord x2, Coord y2)
{
    Point p1(x1, y1);
    Point p2(x2, y2);

    p1 += _origin;
    p2 += _origin;

    p->begin(screen);
    p->setPen(QColor(_r,_g,_b));
    p->drawLine(QLine(p1._x, p1._y, p2._x, p2._y));
    p->end();
}

void PainterQt::drawTriangle(const Point3 &p1, const Point3 &p2, const Point3 &p3)
{
    QPoint points[3];

    points[0].setX(p1._x + _origin._x);
    points[0].setY(p1._y + _origin._y);

    points[1].setX(p2._x + _origin._x);
    points[1].setY(p2._y + _origin._y);

    points[2].setX(p3._x + _origin._x);
    points[2].setY(p3._y + _origin._y);

    p->begin(screen);
    p->setCompositionMode(QPainter::CompositionMode_Darken);
    p->setPen(Qt::NoPen);
    p->setBrush(QColor((INT16S)_r*p1._z>>11, (INT16S)_g*p1._z>>11,  255));
    p->drawPolygon(points, 3);
    p->end();
}

//PainterQt::enableDepthTest();

void PainterQt::drawText(Coord x, Coord y, const Str &str)
{
    p->begin(screen);
    p->setPen(QColor(0,0,0));

    p->drawText(_origin._x + x, _origin._y + y, QString(str._data));
    p->end();
}

void PainterQt::fillRect(Coord x, Coord y, Coord width, Coord height, const Color &color)
{
    p->begin(screen);
    p->fillRect(_origin._x + x, _origin._y + y, width, height, QColor(color._r, color._g, color._b));
    p->end();
}

void PainterQt::flush()
{
    gWidget->setImage(screen);
}
