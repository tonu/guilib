#include <ucos_ii.h>
#include <stdlib.h>
#include <ioavr.h>

#include "display.h"
#include "spics.h"
#include "spi.h"
#include "pin.h"
#include "spics.h"

#define BYTE(b7,b6,b5,b4,b3,b2,b1,b0) (b7<<7 | b6<<6 | b5<<5 | b4<<4 | b3<<3 | b2<<2 | b1<<1 | b0)

// see the last page in thist doc: http://www.farnell.com/datasheets/31870.pdf
#define        TurnOff        BYTE(1,0,1,0,1,1,1,0)
#define        TurnOn         BYTE(1,0,1,0,1,1,1,1)
#define        ADCSelectNorm  BYTE(1,0,1,0,0,0,0,0)
#define        ADCSelectRev   BYTE(1,0,1,0,0,0,0,1)
#define        DispNorm       BYTE(1,0,1,0,0,1,1,0)
#define        DispRev        BYTE(1,0,1,0,0,1,1,1)
#define        AllPointsOff   BYTE(1,0,1,0,0,1,0,0)
#define        AllPointsOn    BYTE(1,0,1,0,0,1,0,1)
#define        RMWStart       BYTE(1,1,1,0,0,0,0,0) // starts Read/modify/write - increments address on write
#define        RMWEnd         BYTE(1,1,1,0,1,1,1,0)
#define        Bias0          BYTE(1,0,1,0,0,0,1,0)
#define        Bias1          BYTE(1,0,1,0,0,0,1,1)
#define        Reset          BYTE(1,1,1,0,0,0,1,0)
#define        ComOutNorm     BYTE(1,1,0,0,0,0,0,0)
#define        ComOutRev      BYTE(1,1,0,0,1,0,0,0)
#define        PowerCtrl      BYTE(0,0,1,0,1,0,0,0) // or cmd with internal power supply mode value (0-7)
#define        ResistorCtrl   BYTE(0,0,1,0,0,0,0,0) // or cmd with resistor ratio (0-7)
#define        ElecVolCtrl    BYTE(1,0,0,0,0,0,0,1) // after this send v5 output voltage electronic volume (0-63)


// iar bugi.. juhul kui on olemas ainult forward declaration nt class Spi; siis new Spi() ei anna errorit .. fakk
extern Spi *gSpiE;

// S1D10605D04B - display driver
Display::Display()
{
    displayBuf = (char*)malloc(128*8);
    
    initMCUHardware(); // initializes MCU pins for LCD
    resetLCD();  // does a hw reset on lcd

    spics = new SpiCS(); // lcd chipselect

    spics->setPin(csPin);
    spics->setCtrlReg(SPI_MODE_3_gc); // MSB, mode0, clkper/4, No SPI Clock Double

    gSpiE->addCS(spics);

    initLCD();
}

void Display::initLCD()
{
    commandPin->clr(); // LCD to command mode

    spics->send(Reset);
    spics->send(ADCSelectRev);
    spics->send(ComOutNorm);
    spics->send(DispNorm);
    spics->send(AllPointsOff);
    spics->send(ResistorCtrl | 4);
    spics->send(Bias0);
    spics->send(PowerCtrl | 7);
    spics->send(ElecVolCtrl);
    spics->send(43);// miks n2ites selline lambiarv oli?
    spics->send(TurnOn);
}

void Display::initMCUHardware()
{
    resetPin = new Pin(&PORTQ, PIN0_bp, Pin::Output);
    csPin = new Pin(&PORTQ, PIN1_bp, Pin::Output);
    commandPin = new Pin(&PORTQ, PIN2_bp, Pin::Output);
    rdPin = new Pin(&PORTQ, PIN3_bp, Pin::Output);

    resetPin->set();
    csPin->set();
    commandPin->set();
    rdPin->set();
}

void Display::resetLCD()
{
    OSTimeDly(1);
    resetPin->clr(); OSTimeDly(1);
    resetPin->set(); OSTimeDly(1);
}

void display_dma_transfer(void * src, void * dst)
{
    DMA.CTRL |= DMA_ENABLE_bm; 

    DMA.CH0.CTRLA |= DMA_CH_SINGLE_bm; // SINGLE burst

    DMA.CH0.CTRLA &= ~DMA_CH_BURSTLEN0_bm; //1 byte burstlen
    DMA.CH0.CTRLA &= ~DMA_CH_BURSTLEN1_bm;

    DMA.CH0.ADDRCTRL = 0;
    DMA.CH0.ADDRCTRL |= DMA_CH_SRCDIR0_bm; //increment
    DMA.CH0.ADDRCTRL |= DMA_CH_DESTRELOAD1_bm;// after every burst load initial

    DMA.CH0.TRIGSRC = 0x8A; //autotrigger from spie

    DMA.CH0.TRFCNT = 128;//

    DMA.CH0.SRCADDR0 = ((INT32U)src >> 0) & 0xFF;
    DMA.CH0.SRCADDR1 = ((INT32U)src >> 8) & 0xFF;
    DMA.CH0.SRCADDR2 = ((INT32U)src >> 16) & 0xFF;

    DMA.CH0.DESTADDR0 = ((INT32U)dst>> 0) & 0xFF;
    DMA.CH0.DESTADDR1 = ((INT32U)dst >> 8) & 0xFF;
    DMA.CH0.DESTADDR2 = ((INT32U)dst >> 16) & 0xFF;

    DMA.CH0.CTRLA |= DMA_CH_ENABLE_bm;//en
    DMA.CH0.CTRLA |= DMA_CH_TRFREQ_bm;

    while (!(DMA.INTFLAGS & DMA_CH0TRNIF_bm))
        OSTimeDly(1);

    DMA.INTFLAGS |= DMA_CH0TRNIF_bm;
}

void Display::blit(const char *data)
{
    INT16U page;

    // move from framebuf to dsplaybuf
 
  //  INT16U index = (p._y>>3) * 128;
  //  frameBuf[index + p._x] |= 1<<(p._y & 0x07);
         
    for(page=0;page<8;page++)
    {
        for (INT16U i=0; i<128; i++) {
          displayBuf[page*128 + i] =
              (data[(page*8+0)*128 + i]?1:0) |
              (data[(page*8+1)*128 + i]?2:0) |
              (data[(page*8+2)*128 + i]?4:0) |
              (data[(page*8+3)*128 + i]?8:0) |
              (data[(page*8+4)*128 + i]?16:0) |
              (data[(page*8+5)*128 + i]?32:0) |
              (data[(page*8+6)*128 + i]?64:0) |
              (data[(page*8+7)*128 + i]?128:0) ;
        }

        commandPin->clr(); // LCD to command mode

        spics->send(0x00);  //Column adress lower nibble
        spics->send(0x10);  //Column adress higher nibble
        spics->send(0xB0+page);

        commandPin->set(); // LCD to data mode

        csPin->clr(); // lcd chipselect
        display_dma_transfer(displayBuf + page*128, (char*)&SPIE.DATA);
        csPin->set();
    }
}
