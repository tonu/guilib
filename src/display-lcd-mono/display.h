#ifndef DISPLAY_H
#define DISPLAY_H


#include <ucos_ii.h>

#include "guilib/util.h"// types
class SpiCS;
class Pin;

class Display
{
public:

Display();

    void initMCUHardware();
    void resetLCD();
    void initLCD();

    void blit(const char *data);
    
private:

    char * displayBuf;

    Pin *resetPin;
    Pin *commandPin;
    Pin *csPin;
    Pin *rdPin;

    SpiCS *spics;
};

#endif
