#include "appucos.h"
#include "event.h"
#include "wi.h"
#include "listwi.h"
#include "paintermono.h"
#include "glwi.h"
#include "widgets/analogtestwi.h"
#include <includes.h>

#define EVENT_QUEUE_SIZE 10
Event *eventQueueStorage[EVENT_QUEUE_SIZE];
OS_EVENT * eventQueue;

Wi *w11;
AppUCOS::AppUCOS()
    :Application()
{
    /*
    eventQueue = OSQCreate((void**)eventQueueStorage, EVENT_QUEUE_SIZE);

    Wi *_topWidget = new Wi(0, new Layout(Layout::Vertical));

    w11 = new GLWi(_topWidget);
    Wi *w12 = new AnalogTestWi(_topWidget);
   
    
    Focus *foc = new Focus;
    w11->setFocus(foc);
    w12->setFocus(foc);
    w12->grabFocus();

    _topWidget->setRect(Rect(0,0, 128, 64));
    _topWidget->doLayout();
    */
        
    Wi * _topWidget = new Wi(0, new Layout());

    Focus *foc = new Focus;


///   Wi *w1 = new GLWi(_topWidget);
    Wi *w2 = new Wi(_topWidget, new Layout(Layout::Horizontal));

    Wi *w21 = new ListWi(w2);
    Wi *w22 = new ListWi(w2);

 ///   w1->setFocus(foc);
    w21->setFocus(foc);
    w22->setFocus(foc);

    //w1->grabFocus();

    _topWidget->setRect(Rect(0,0, 128, 64));
    _topWidget->doLayout();

    w2->doLayout();

}

bool AppUCOS::post(Event *event)
{
    INT8U ret;
    ret = OSQPost (eventQueue, event);

    if (ret != OS_NO_ERR) {
        // APP_TRACE_INFO(((INT8U*)"error: %u\n\r", ret));
    }

    return true;
}

void AppUCOS::run()
{
    execute();
}

Painter *AppUCOS::painter()
{
    if (_painter == 0)
        _painter = new PainterMono;

    return _painter;
}


Event * AppUCOS::pendEvent()
{
  INT8U err;
    Event * ev;
    
    ev = (Event *)OSQPend (eventQueue, 50, &err); // pend until something happens - wait forever
    
    if (err != 0)
        ev = NULL;

  //  if (ev == 0)
 //       w11->update();
    
    return ev;
}

