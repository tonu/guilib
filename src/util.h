#ifndef UTIL_H
#define UTIL_H

//http://stackoverflow.com/questions/13834367/why-semaphore-object-is-not-initialized

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#ifdef QT_GUI_LIB


typedef unsigned char INT8U;
typedef char INT8S;

typedef unsigned short int INT16U;
typedef short int INT16S;

typedef unsigned  int INT32U;
typedef  int INT32S;

typedef INT16S Coord; // screen pixel coord or width/height. type depends on screen size

#define IAR_FLASH

#else

#include <ucos_ii.h>
typedef INT16S Coord;

#define IAR_FLASH __flash
#endif

class Rect
{
public:
    inline Rect() { _x = 0; _y = 0; _w = 0; _h = 0; }
    inline Rect(int x, int y, int w, int h) { _x = x; _y = y; _w = w; _h = h; }

    int _x;
    int _y;
    int _w;
    int _h;
};

class Point
{
public:
    Point() { _x = 0; _y = 0; }
    Point(Coord x, Coord y) { _x = x; _y = y; }

    Point & operator+=(const Point &p)  { _x+=p._x; _y+=p._y; return *this; }

    Coord _x;
    Coord _y;
};
class Point3 :public Point
{
public:
    Point3() { _x = 0; _y = 0; _z = 0; }
    Point3(Coord x, Coord y, Coord z) { _x = x; _y = y; _z = z; }

    Point3 & operator+=(const Point3 &p)  { _x+=p._x; _y+=p._y; _z+=p._z; return *this; }

    Coord _z;

};

////////////////////
///  Color
////////////////////
class Color
{
public:
    inline Color(int r, int g, int b) { _r=r; _g=g; _b=b; }

    
    INT8U mono() const { return ( _r==0?0xff:0); }  // if r has value 0 then output "black" pixel
    
    int _r;
    int _g;
    int _b;
};



////////////////////
///  List
////////////////////
template <class T>
        class List
{
public:

    class ConstIterator
    {
    public:
        ConstIterator(const List<T> *list)
            :
            _list(list),
            _ptr(list->_data)
        {}

        inline T & operator*() {
            return *_ptr;
        }
        inline void operator+=(int count) {
            _ptr += count;
        }
        inline void operator++ (int) { //Note: C++ uses the unnamed dummy-parameter ?int? to differentiate between prefix and Suffix increment operators.
            _ptr ++;
        }
        inline bool isValid() const {
            return (_ptr > (_list->_data + _list->_count - 1) || _ptr < _list->_data ? false : true);
        }

    private:
        const List<T> *_list;
        T *_ptr;
    };
    class Iterator
    {
    public:
        Iterator(List<T> *list)
            :
            _list(list),
            _ptr(list->_data)
        { }

        inline T & operator*() {
            return *_ptr;
        }
        inline void operator+=(int count) {
            _ptr += count;
        }
        inline void operator++() {
            _ptr ++;
        }
        inline bool isValid() const {
            return (_ptr > (_list->_data + _list->_count - 1) || _ptr < _list->_data ? false : true);
        }

    private:
        List<T> *_list;
        T *_ptr;
    };
    Iterator iterator() { return Iterator(this); }
    ConstIterator constIterator() const { return ConstIterator(this); }

    inline List(int initialSize = 2) {
        _size = 0; _count = 0; _data = 0;
        resize(initialSize);
    }
    List(const List & rhs)// a copy constructor
    {
        *this = rhs;
    }

    ~List() { if (_data) free(_data); }


    List<T> & operator=(const List<T> &rhs)
                       {
        _size = rhs._size;
        _count = rhs._count;
        _data = (T*)malloc(sizeof(T) * _size);

        memcpy(_data, rhs._data, sizeof(T) * _size);  // move data

        return *this;
    }

    void append(const T &item);
    int  count() const;
    const T & at(int i) const;
    T & operator[](int i);
    void resize(int size);
    void removeLast()
    {
        if (_count>0)
            _count--;
    }
    T takeLast()
    {
        if (_count>0)
            _count--;

        return _data[_count];
    }

    void load(const T *data, int count);

    T *_data;


    int _size;
    int _count;
protected:
    static T _safety;
};

template <typename T>
        T List<T>::_safety;


template <typename T>
        void List<T>::load(const T *data, int count)
{
    if (_size < count)
        resize(count);

    memcpy(_data, data, sizeof(T) * count);

    _count = count;
}

template <typename T>
        inline void List<T>::append(const T &item)
{
    if (_count >= _size)   // need to expand
        resize(_size * 2); // make the list twice as big

    _data[_count] = item;

    _count++;
}


template <typename T>
        inline void List<T>::resize(int size)
{
    if (_count >= size) // if we want to make list smaller than the count of items, dont do anything.
        return;

    //allocate mem for new data
    T *_newData = (T*)malloc(sizeof(T) * size);

    //if there is old data
    if (_data)
    {
        memcpy(_newData, _data, sizeof(T) * _count);  // move data
        free(_data); // now free old
    }

    _size = size;
    _data = _newData; // set _data to new address
}

template <typename T>
        inline const T & List<T>::at(int i) const
{
    if (i<_count && i>=0 && _data)
        return _data[i];

    //a safety net
    return _safety;
}
template <typename T>
        inline T & List<T>::operator[](int i)
{
    if (i<_count && _data)
        return _data[i];

    //a safety net
    return _safety;
}


template <typename T>
        inline int List<T>::count() const
{
    return _count;
}




////////////////////
///  Str
////////////////////
class Str : public List<char>
{
public:
    Str(int initialSize = 2)
        :
        List<char> (initialSize)
    {
        _data[0] = 0;
        _count = 1;
    }
    
    Str(const char * string) : List<char> ()
    {
        set(string);
    }

    void set(const char *str)
    {
        int len = strlen(str) + 1; // +1 because terminating NULL

        if (len > _size)
            resize(len);

        strcpy(_data, str);

        _count = len; // dont count null term
    }

    void removeLast()
    {
        if (_count < 2)
            return;

        List<char>::removeLast();
        _data[_count-1] =0; // terminaator        
    }

    void append (const Str &str)
    {
        INT16U totalCount = _count+str._count;

        if (totalCount-1 > _size) // -1 because "aa\0"+"bb\0" doesnt need 6 chars but 5
            resize (totalCount);

        memcpy(_data+_count-1, str._data, sizeof(char) * str._count);

        _data[totalCount- 1] =0;
        _count += str._count -1;
    }

    bool operator ==(const Str& b) const
    {
        return strcmp(_data, b._data) == 0;
    }

    Str operator +(const char *rhs) const
    {
        Str tmp(_count + strlen(rhs)+1);
        tmp.append(*this);
        tmp.append(rhs);

        return tmp;
    }

    Str & operator +=(const Str& b)
    {
         append(b);
         return *this;
    }

};

Str operator +(const Str& lhs, const Str& rhs);

/*

template <typename T>
        T max( T a, T b) {
        return (a>b?a:b);
}

template <typename T>
        T min (T a, T b) {
    return (a<b?a:b);
}
*/



////////////////////
///  RingBuf
////////////////////
template <typename T>
        class RingBuf
{
public:
    RingBuf(INT16U size);
    ~RingBuf();

    void put(const T & v);
    T get();

    INT16U _begin;
    INT16U _end;
    INT16U _size;
    INT16U _count;

private:
    void advance(INT16U &v);


    T *_data;
    T _empty;
    sem_t *sem;
    pthread_mutex_t mutex;
};
template <typename T>
        RingBuf<T>::RingBuf(INT16U size)
{
    _begin = _end = 0;

    _size = size;
    _data = (T*)malloc(sizeof(T) * size);

    _count = 0;

//win    if(sem_init(&sem, 0, 0))
    if ((sem = sem_open("/tmp/mysem",O_CREAT,0644,0)) == SEM_FAILED)
    {
        printf("Could not initialize a semaphore\n");
        return;
    }
    if(pthread_mutex_init(&mutex, NULL))
    {
        printf("Unable to initialize a mutex\n");
        return;
    }
}

template <typename T>
        RingBuf<T>::~RingBuf()
{
    free((void*)_data);
    sem_close(sem);
    pthread_mutex_destroy(&mutex);
}

template <typename T>
        void RingBuf<T>::put(const T & v)
{
    pthread_mutex_lock(&mutex);

    if (_count == _size)
    {
        return; // full
    }

    _count++;
    _data[_end] = v;

    advance(_end);

    pthread_mutex_unlock(&mutex);

    sem_post(sem);
}

template <typename T>
        T RingBuf<T>::get()
{
    sem_wait(sem);
    pthread_mutex_lock(&mutex);

    INT16U tmp = _begin;
    advance(_begin);
    _count--;

    pthread_mutex_unlock(&mutex);

    return _data[tmp];// FIXME
}

template <typename T>
        inline void RingBuf<T>::advance(INT16U &v)
{
    v++;
    if (v == _size)
        v=0;
}


#endif // UTIL_H
