#ifndef VEC_H
#define VEC_H

#include "util.h"

class Vec: public List<INT32>
{
public:

    Vec(INT8U cols, INT8U rows):
            List<INT32>(rows*cols)
    { _cols = cols; _rows = rows; _count = rows*cols; }

    Vec operator*(const Vec & left) const {

        if (left._rows != this->_cols)
            return Vec(0,0); // error!

        INT8U rows = min(_rows, left._rows);
        INT8U cols = min(_cols, left._cols);

        Vec v(cols, rows);
        Iterator itResult(v.iterator());

        ConstIterator rowIt(this->constIterator());
        for (INT8U r=0; r<rows;r++) {

            ConstIterator colIt(left.constIterator());
            for (INT8U c=0; c<cols;c++) {

                ConstIterator a(rowIt);
                ConstIterator b(colIt);
                INT32 acc = 0;

                for (int i=0; i<_cols; i++) {
                    acc += ((*a) * (*b));

                    a+=1;
                    b+=left._cols;
                }
                *itResult = acc  >>16;

                itResult += 1;
                colIt +=1;
            }
            rowIt += this->_cols;
        }

        return v;
    }
/*
    INT32 colSum(INT8U col)
    { // possible to cache

        INT32 acc = 0;
        Iterator it = iterator();
        it += col;
        qDebug() << "col" << col;
        for (int i=0; i<_rows;i++) {

            acc += *it;
            qDebug() << acc;
            it += _cols;
        }
        qDebug() << "col done!";
        return acc;

    }
    INT32 rowSum(INT8U row)
    {
        INT32 acc = 0;
        Iterator it(iterator());
        it += row * _cols;
        qDebug() << "row" << row;
        for (int i=0; i<_cols;i++) {

            acc += *it;
            qDebug() << acc;
            it += 1;
        }
        qDebug() << "row done!";
        return acc;
    }
    */

    INT8U _rows;
    INT8U _cols;
};

#endif // VEC_H
