#ifndef APP_H
#define APP_H

#include "application.h"
#include "util.h"

#include <semaphore.h>
#include <pthread.h>

class App : public Application
{
public:
    App();
    ~App();

    void post(Event *event); // event
    Painter * painter();
    Event * pendEvent();
    virtual void run();

private:
    RingBuf<Event*> eventList;
};

#endif // APP_H
