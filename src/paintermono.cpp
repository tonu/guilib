#include "paintermono.h"

//#include "font6x8.h"
#include "font5x7.h"
#include "wi.h"

#if QT_GUI_LIB
#include "gldisplay.h"
#else
#include "devices/monochromedisplay.h"

#endif

PainterMono::PainterMono()
{
    memset((void *)&_color, 0, sizeof (struct color));

#ifdef QT_GUI_LIB
    _frameBuf = (struct color *)malloc(sizeof(struct color) * 3* SCREEN_WIDTH*SCREEN_HEIGHT);
    _display = new GLDisplay;
#else
    _frameBuf = (unsigned char*)malloc(sizeof(unsigned char) * SCREEN_WIDTH*SCREEN_HEIGHT);
    _display = new MonochromeDisplay;
#endif
}


void PainterMono::setOrigin(const Point &p)
{
    _origin = p;
}

void PainterMono::setColor(INT8U r, INT8U g, INT8U b)
{
    _color.r = r;
    _color.g = g;
    _color.b = b;
}

void PainterMono::drawLine(const Point &p1, const Point &p2)
{
    drawLine(p1._x,p1._y, p2._x,p2._y);
}

void PainterMono::drawLine(Coord x1, Coord y1, Coord x2, Coord y2)
{
    x1+= _origin._x;
    x2+= _origin._x;

    y1+= _origin._y;
    y2+= _origin._y;

    int dx, dy, i, e;
    int incx, incy, inc1, inc2;
    int x, y;

    /*debug: printf("\n(%d,%d)->(%d,%d): ", x1,y1,x2,y2); */
    dx = x2 - x1;
    dy = y2 - y1;
    incx = 1;
    incy = 1;
    if (dx < 0) {
        dx = -dx;
        incx = -1;
    }
    if (dy < 0) {
        dy = -dy;
        incy = -1;
    }
    x = x1;
    y = y1;

    // value = (char *)&data;

    if (dx > dy) {
        drawPoint(x,y);
        /*debug: printf("put1-(%d,%d) ",x,y); */
        e = 2 * dy - dx;
        inc1 = 2 * (dy - dx);
        inc2 = 2 * dy;
        for (i = 0; i < dx - 1; i++) {
            if (e >= 0) {
                y += incy;
                e += inc1;
            }
            else
                e += inc2;
            x += incx;
            drawPoint(x,y);
            /*debug:printf("put2-(%d,%d) ",x,y); */
        }
    }
    else {
        drawPoint(x,y);
        /*debug:printf("put3-(%d,%d) ",x,y); */
        e = 2 * dx - dy;
        inc1 = 2 * (dx - dy);
        inc2 = 2 * dx;
        for (i = 0; i < dy - 1; i++) {
            if (e >= 0) {
                x += incx;
                e += inc1;
            }
            else
                e += inc2;
            y += incy;
            drawPoint(x,y);
            /*debug:rintf("put4-(%d,%d) ",x,y); */
        }
    }

}

void PainterMono::drawText(Coord x, Coord y, const Str &str)
{
    x+= _origin._x;
    y+= _origin._y;

    Str::ConstIterator it(&str);
    while (it.isValid())
    {
        //find index
        INT8U index = *it - 0x20;

        /* 6x8 kood*

        if (index <= (font6x8LUTCount-1)) // if this char is not in LUT
        {
            for (INT8U row=0; row<8; row++)
            {
                INT8U rowData;

                //load from lookup table
                rowData = font6x8LUT[index][row];

                // paint each pixel
                for (INT8U col=0; col<6; col++)
                {
                    if (rowData & (1<<(7-col))) // + 2 because font not 8 pixel wide, but 6
                    {
                        drawPoint(x+col, y+row);
                    }
                }
            }
        }
        x+=7;

*/
        if (index <= (font_5x7_count-1)) // if this char is not in LUT
        {
            for (INT8U row=0; row<7; row++)
            {
                INT8U bit = 1<<row;
                const INT8U IAR_FLASH *colData = &font_5x7_data[index*5];


                for (INT8U col=0; col<5; col++)
                    if (*colData++ & bit)
                        drawPoint(x+col, y+row);
            }
        }
        x+=6;
        it += 1;
    }
}

void PainterMono::drawTriangle(const Point3 &p1, const Point3 &p2, const Point3 &p3)
{
    drawLine(p1,p2);
    drawLine(p2,p3);
    drawLine(p3,p1);
}


void PainterMono::fillRect(Coord x, Coord y, Coord width, Coord height)
{
    x += _origin._x;
    y += _origin._y;

    for (Coord h=0; h< height; h++) // fixme 640x480... will be big number
    {
        drawHorLine(x, width, y++);
    }
}

void PainterMono::setClipRect(Coord x, Coord y, Coord width, Coord height)
{
    x += _origin._x;
    y += _origin._y;

    _clipLeft = x;
    _clipRight = x+width;

    _clipTop = y;
    _clipBottom = y+height;
}

void PainterMono::flush()
{
    _display->blit((unsigned char*)_frameBuf);
}


// private [in screen coords (not widget
inline void PainterMono::drawPoint(const Point &p)
{
    drawPoint(p._x, p._y);
}

inline void PainterMono::drawPoint(Coord x, Coord y)
{
    if (x >= _clipLeft && x <= _clipRight &&
        y >= _clipTop &&  y <= _clipBottom)
    {
        _frameBuf[(INT16U)y * SCREEN_WIDTH + x] = _color;
    }
}

inline void PainterMono::drawHorLine(Coord x, Coord width,  Coord y)
{
    for (Coord i=0; i< width; i++)
    {
        drawPoint(x++, y);
    }
}
