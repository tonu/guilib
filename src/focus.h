#ifndef FOCUS_H
#define FOCUS_H

#include "util.h"

class Wi;

class Focus
{
public:
    Focus();

    void add(Wi *widget);
    Wi * getNext();
    Wi * getPrev();

private:
    List<Wi*> _list;
    int _index;
};

#endif // FOCUS_H
