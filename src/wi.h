#ifndef WI_H
#define WI_H

#include "util.h"

#include "event.h"
#include "layout.h"
#include "obj.h"
#include "focus.h"

class Painter;

class Wi : public Obj
{
public:
    Wi(Wi *parent = 0, Layout *layout = 0);

    static Wi * focusPrev();
    static Wi * focusNext();

    // default event handler. If subclassed for special event type ie paint
    // then call this at the end for all other types than paint
    virtual bool handleEvent(Event *ev);
    virtual void paintEvent();
    virtual void keyPressEvent(KeyEventData *dat);
    virtual void keyReleaseEvent(KeyEventData *dat);

    void update();
    void doLayout();
    void grabFocus();
    void loseFocus();
    void render();

    void setFocus(Focus *foc);

    void setRect(const Rect &r); // for reading rect, just read _rect ivar
    void setRect(int x, int y, int w, int h) { setRect(Rect(x,y,w,h)); }

    Point mapToGlobal(int x, int y);


    Wi *parent () { return _parent; }

// All these instance vars are public for READING fast. None of them should be written to!
    Wi *_parent;
    Layout *_layout;

    Focus *_foc;  // if nonNull, its focusable
    bool _hasFocus;

    List<Wi*> _children;
    Rect _rect;

    static Wi *_keyboardGrabberWidget;
    static void setKeyboardGrabber(Wi *widget);
protected:
    void addChild(Wi *child);

    static Painter *painter;
    static Wi * _topLevel;
private:
};

#endif // WI_H
