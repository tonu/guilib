#ifndef LAYOUT_H
#define LAYOUT_H

#include "util.h"

class Wi;

class Layout
{
public:
    enum Orientation {
        Vertical,
        Horizontal
    };

    Layout(Orientation orient = Vertical);

    void setChildList(List<Wi*> * widgets){ _widgets = widgets; }

    void exec();

private:
    List<Wi*> *_widgets;
    Orientation _orient;
};

#endif // LAYOUT_H
