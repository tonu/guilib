#ifndef PAINTERMONO_H
#define PAINTERMONO_H

#include "painter.h"
#include "util.h"

#define SCREEN_WIDTH 160*4
#define SCREEN_HEIGHT 128*4

class Wi;
class Display;

struct color {
    INT8U r;
    INT8U g;
    INT8U b;
};

class PainterMono : public Painter
{
public:
    PainterMono();

    void setOrigin(const Point &p);
    void drawLine(Coord x1, Coord y1, Coord x2, Coord y2);
    void drawLine(const Point &p1, const Point &p2);
    void setColor(INT8U r, INT8U g, INT8U b);
    void drawText(Coord x, Coord y, const Str &str);
    void fillRect(Coord x, Coord y, Coord width, Coord height);
    void setClipRect(Coord x, Coord y, Coord width, Coord height);
    void drawTriangle(const Point3 &p1, const Point3 &p2, const Point3 &p3);

    inline void drawHorLine(Coord x, Coord width,  Coord y);
    inline void drawPoint(const Point &p);
    inline void drawPoint(Coord x, Coord y);

    void flush();

private:
    struct color _color;
    struct color * _frameBuf;

    Coord _clipLeft;
    Coord _clipRight;
    Coord _clipTop;
    Coord _clipBottom;

    Display *_display;
};



#endif // PAINTER_H
