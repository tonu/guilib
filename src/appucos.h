#ifndef APPUCOS_H
#define APPUCOS_H

#include "application.h"

class AppUCOS : public Application
{
public:
    AppUCOS();

    bool post(Event *event); // event
    Painter * painter();
    Event * pendEvent();
    virtual void run();
};


#endif // APPUCOS_H
