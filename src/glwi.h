#ifndef GLWI_H
#define GLWI_H

//#include <ucos_ii.h>// siit aint tyype vaja
#include "wi.h"
#include "matrix.h"



typedef struct {
    INT16S x;
    INT16S y;
    INT16S z;
  //  INT16S w;
} point3;

class GLWi : public Wi
{
public:
    GLWi(Wi *parent = 0, Layout *layout = 0);

    virtual void paintEvent();

    virtual void keyPressEvent(KeyEventData *dat);
    virtual void keyReleaseEvent(KeyEventData *dat);

    static bool isClockwise(const Point3 &p1, const Point3 &p2, const Point3 &p3);

private:
    void transform(const point3 IAR_FLASH &vert, Point3 &out);

    Matrix<4,4> proj;
    Matrix<4,4> model;
    Matrix<4,4> projAndModel;
    Matrix<1,4> vertIn;
    Matrix<1,4> vertOut;
};


#endif // GLWI_H
