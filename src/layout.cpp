#include "layout.h"
#include "wi.h"

Layout::Layout(Orientation orient)
{
    _orient = orient;
}

void Layout::exec() // test function .. executes vertical layout
{
    int widgetCount = _widgets->count();

    if (widgetCount > 0)
    {
        Wi *parent = _widgets->at(0)->parent();

        if (parent != 0)
        {
            Rect parentRect = parent->_rect;


            int width = parentRect._w;
            int height = parentRect._h;

            if (_orient == Horizontal)
            {
                width = width << 4; // integer scaling
                width /= widgetCount;
                for (int i=0; i<widgetCount; i++)
                {
                    Rect r(((width + 8)*i)>>4, 0,  ((width+8)>>4)-1, height-1);
                    Wi * w = _widgets->at(i);

                    if (w)
                        w->setRect(r);
                }
            }else{
                height = height << 4;
                height /= widgetCount;
                for (int i=0; i<widgetCount; i++)
                {
                    Rect r(0, ((height+8)*i) >>4  , width-1, ((height+8)>>4)-1);
                    Wi * w = _widgets->at(i);

                    if (w)
                        w->setRect(r);
                }
            }
        }
    }
}

