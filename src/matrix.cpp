#include "matrix.h"
#include "sinlut.h"
#include "util.h"


template <INT8U COLS, INT8U ROWS>
        void Matrix<COLS,ROWS>::loadIdent()
{
    memset(_data, 0, sizeof(INT16S) * COLS*ROWS);

    INT16S *ptr = _data;
    for (INT8U i=0;i< COLS; i++)
    {
        *ptr = 256;
        ptr += COLS + 1;
    }
}

template <>
        void Matrix<4,4>::load(const INT16S * data)
{
    memcpy(_data, data, sizeof(INT16S) * 16);
}

template <INT8U COLS, INT8U ROWS>
        void Matrix<COLS,ROWS>::load(const Matrix<COLS,ROWS> & src)
{    
    memcpy(_data, src._data, sizeof(INT16S) * (COLS * ROWS));
}

template <INT8U COLS, INT8U ROWS>
        void Matrix<COLS,ROWS>::mult(const Matrix<COLS,ROWS> &right)
{
    Matrix<COLS,ROWS> m;

    mult(right, m);

    load(m);
}
#if 0
template <INT8U COLS, INT8U ROWS>
        void Matrix<COLS,ROWS>::mult(const Matrix &right, Matrix & output) const
{
    if (right._rows != _cols)
        return ; // error!

    INT16S *out = output._data;

    INT8U rows = min(_rows, right._rows);
    INT8U cols = min(_cols, right._cols);

    INT16S *rowPtr = _data;
    for (INT8U rr=0; rr<rows;rr++) {

        INT16S *colPtr = right._data;
        for (INT8U cc=0; cc<cols;cc++) {

            INT16S *l = rowPtr;
            INT16S *r = colPtr;

            INT32S acc = 0;
            for (int i=0; i<_cols; i++) {
                acc += ((INT32S)(*l) * (*r)) ;

                l++;
                r+=right._cols;
            }
            *out = acc >> 8;

            out++;
            colPtr++;
        }
        rowPtr += _cols;
    }

    output._cols = cols;
    output._rows = rows;
}
#endif


template <>
        void Matrix<4,4>::mult(const Matrix<1,4> &right, Matrix<1,4> & output) const
{
    INT16S *out = output._data;

    const INT16S *rowPtr = _data;
    for (INT8U row=0; row<4;row++) {

        const INT16S *l = rowPtr;
        const INT16S *r = right._data;

        INT32S acc = 0;
        acc += ((INT32S)(*l++) * (*r++)) ;
        acc += ((INT32S)(*l++) * (*r++)) ;
        acc += ((INT32S)(*l++) * (*r++)) ;
        acc += ((INT32S)(*l) * (*r)) ;
        *out = acc >> 8;

        out++;
        rowPtr += 4;
    }



}
template <>
        void Matrix<4,4>::mult(const Matrix<4,4> &right, Matrix<4,4> & output) const
{

    INT16S *out = output._data;

    const INT16S *rowPtr = _data;
    for (INT8U rr=0; rr<4;rr++) {

        const INT16S *colPtr = right._data;
        for (INT8U cc=0; cc<4;cc++) {

            const INT16S *l = rowPtr;
            const INT16S *r = colPtr;

            INT32S acc = 0;
            acc += ((INT32S)(*l++) * (*r)) ; r+=4;
            acc += ((INT32S)(*l++) * (*r)) ; r+=4;
            acc += ((INT32S)(*l++) * (*r)) ; r+=4;
            acc += ((INT32S)(*l++) * (*r)) ;
            *out = acc >> 8;

            out++;
            colPtr++;
        }
        rowPtr += 4;
    }

}

template <>
        void Matrix<4,4>::scale( INT16S xScale, INT16S yScale, INT16S zScale)
{
    Matrix<4,4> tmp;
    tmp.loadIdent();

    tmp._data[0] = xScale;
    tmp._data[5] = yScale;
    tmp._data[10] = zScale;

    mult(tmp);
}

template <>
        void Matrix<4,4>::translate( INT16S tx, INT16S ty, INT16S tz)
{
    Matrix<4,4> tmp;
    tmp.loadIdent();

    tmp._data[3] = tx;
    tmp._data[7] = ty;
    tmp._data[11] = tz;

    mult(tmp);
}

// right handed rotation on x axis - //http://www.cprogramming.com/tutorial/3d/rotationMatrices.html
template <>
        void Matrix<4,4>::rotate( INT16S xRot, INT16S yRot, INT16S zRot)
{
    Matrix<4,4> tmpX;
    Matrix<4,4> tmpY;
    Matrix<4,4> tmpZ;

    tmpX.loadIdent();
    tmpY.loadIdent();
    tmpZ.loadIdent();

    tmpX._data[10] = tmpX._data[5] = (INT16S)cosInt(xRot) << 1 ;//fixme
    tmpX._data[9] = -(tmpX._data[6] = (INT16S)sinInt(xRot)) << 1;

    tmpY._data[10] = tmpY._data[0] = (INT16S)cosInt(yRot) << 1;
    tmpY._data[2] = -(tmpY._data[8] = (INT16S)sinInt(yRot)) << 1;

    tmpZ._data[5] = tmpZ._data[0] =   (INT16S)cosInt(zRot) << 1;
    tmpZ._data[4] = -(tmpZ._data[1] = (INT16S)sinInt(zRot)) << 1;

    tmpY.mult(tmpZ);
    tmpX.mult(tmpY);

    mult(tmpX);
}




