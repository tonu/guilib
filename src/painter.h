#ifndef PAINTER_H
#define PAINTER_H

//#include <ucos_ii.h>

#include "util.h"

class Painter
{
public:
    virtual void setOrigin(const Point &p) = 0;
    virtual void drawLine(Coord x1, Coord y1, Coord x2, Coord y2) = 0;
    virtual void setColor(INT8U r, INT8U g, INT8U b) = 0;
    virtual void drawText(Coord x, Coord y, const Str &str) = 0;
    virtual void fillRect(Coord x, Coord y, Coord width, Coord height) = 0;
    virtual void setClipRect(Coord x, Coord y, Coord width, Coord height)= 0;
    virtual void drawTriangle(const Point3 &p1, const Point3 &p2, const Point3 &p3) = 0;

    virtual void flush()=0;

    void drawLine(const Point &p1, const Point &p2) { drawLine(p1._x, p1._y, p2._x, p2._y); }
protected:
    Point _origin;

    Coord _r;
    Coord _g;
    Coord _b;
};

#endif // PACoordER_H
