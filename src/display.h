#ifndef DISPLAY_H
#define DISPLAY_H

class Display
{
public:

    virtual void blit(const unsigned char *data) = 0;

private:
};

#endif // DISPLAY_H
