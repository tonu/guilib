#include <stdio.h>
#include <pthread.h>

#include <OpenGL/gl.h>
#include <GLUT/glut.h>

#include "app.h"

const unsigned char *_texture_data;

int _width;
int _height;

GLuint texture;

#define GL_TEXTURE_RECTANGLE_ARB 0x84F5

#define SCREEN_WIDTH 160*4
#define SCREEN_HEIGHT 128*4

#define ACTUAL_WIDTH SCREEN_WIDTH
#define ACTUAL_HEIGHT SCREEN_HEIGHT

void redisplay(const unsigned char *data, int w, int h)
{
    _texture_data = data;
    _width = w;
    _height = h;

    glutPostRedisplay();
}

void initGL()
{
    glClearColor(0,0,0,1);

    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_RECTANGLE_ARB, texture );

    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    // the texture wraps over at the edges (repeat)
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );

    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
}

extern "C"
void processNormalKeys(unsigned char key, int x, int y)
{
  switch (key)
  {
    case '\x1B':
      exit(EXIT_SUCCESS);
      break;
  default:
      app->post(new Event(Event::KeyPress, 0, new KeyEventData(key, true)));
  }
}
void processSpecialKeys(int key, int x, int y)
{
    app->post(new Event(Event::KeyPress, 0, new KeyEventData(key | 0x80, true)));
}

extern "C"
void paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glTexImage2D( GL_TEXTURE_RECTANGLE_ARB, // target
                  0, // mipmap level
                  3, //internalformat
                  SCREEN_WIDTH,
                  SCREEN_HEIGHT,
                  0,
                  GL_RGB,
                  GL_UNSIGNED_BYTE,
                  _texture_data);

    glColor3f(1,1,1);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(0.0f, SCREEN_HEIGHT);         glVertex3f(0, 0,  0.0f);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(SCREEN_WIDTH, SCREEN_HEIGHT); glVertex3f(1, 0,  0.0f);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(SCREEN_WIDTH, 0.0f);          glVertex3f(1,  1,  0.0f);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);                  glVertex3f(0,  1,  0.0f);	// Top Left Of The Texture and Quad
    }
    glEnd();
    glFlush();
    glutSwapBuffers();
}

extern "C"
void resizeGL(int w, int h)
{
    glViewport(0,0,w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0, 1.0, 0 , 1.0, -5.0, 5.0);
    glMatrixMode(GL_MODELVIEW);
}


void* thread_entry(void *arg)
{

    App application;
    application.run();

    return NULL;
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);

    glutInitWindowSize(ACTUAL_WIDTH, ACTUAL_WIDTH);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutCreateWindow("GUI Lib");

    glutKeyboardFunc(&processNormalKeys);
    glutSpecialFunc(&processSpecialKeys);

    glutDisplayFunc(&paintGL);
    glutReshapeFunc(&resizeGL);

    initGL();

    pthread_t thread;
    if(pthread_create(&thread, NULL, &thread_entry, NULL))
    {
        printf("Could not create thread\n");
        return -1;
    }

    glutMainLoop();

    if(pthread_join(thread, NULL))
    {
        printf("Could not join thread\n");
        return -1;
    }

    return EXIT_SUCCESS;
}
