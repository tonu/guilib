#include <QKeyEvent>
#include <QLabel>
#include <QTimer>
#include <QDebug>
#include <QPainter>

#define GL_TEXTURE_RECTANGLE_ARB 0x84F5

#include "widget.h"
#include "listwi.h"
#include "focus.h"

Widget *gWidget;
GLuint texture;

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

#define ACTUAL_WIDTH SCREEN_WIDTH*4
#define ACTUAL_HEIGHT SCREEN_HEIGHT*4


Widget::Widget(QWidget *parent) :
        QGLWidget(parent)
{
    gWidget = this;

    _app = new App;
    _app->start(QThread::HighestPriority);

 //   startTimer(20);
    resize(ACTUAL_WIDTH, ACTUAL_HEIGHT);
}

void Widget::initializeGL()
{
    glClearColor(1,1,1,1);

    glGenTextures( 1, &texture );

    glBindTexture( GL_TEXTURE_RECTANGLE_ARB, texture );

    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // the texture wraps over at the edges (repeat)
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP );
    glTexParameterf( GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP );

    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
}

void Widget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glTexImage2D( GL_TEXTURE_RECTANGLE_ARB, // target
                  0, // mipmap level
                  1, //internalformat
                  SCREEN_WIDTH,
                  SCREEN_HEIGHT,
                  0,
                  GL_RED,
                  GL_UNSIGNED_BYTE,
                  _data);

    glColor3f(1,1,1);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(0.0f, SCREEN_HEIGHT);         glVertex3f(0, 0,  0.0f);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(SCREEN_WIDTH, SCREEN_HEIGHT); glVertex3f( ACTUAL_WIDTH, 0,  0.0f);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(SCREEN_WIDTH, 0.0f);          glVertex3f( ACTUAL_WIDTH,  ACTUAL_HEIGHT,  0.0f);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);                  glVertex3f(0,  ACTUAL_HEIGHT,  0.0f);	// Top Left Of The Texture and Quad
    }
    glEnd();
}

void Widget::resizeGL(int width, int height)
{
    glViewport(0,0,width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0.0, width, 0 , height, -5.0, 5.0);
    glMatrixMode(GL_MODELVIEW);
}


void Widget::resizeEvent ( QResizeEvent * event )
{
    QSize s = event->size();

    // todo // send event
}

void Widget::timerEvent(QTimerEvent *event)
{
    _app->post(new Event(Event::KeyPress, 0, new KeyEventData('S', true))); //(void *)event->key()

}

void Widget::keyPressEvent ( QKeyEvent * event )
{
    _app->post(new Event(Event::KeyPress, 0, new KeyEventData(event->key(), true))); //(void *)event->key()
}

void Widget::keyReleaseEvent ( QKeyEvent * event )
{
    _app->post(new Event(Event::KeyRelease, 0, new KeyEventData(event->key(), false)));
}

// called from other thread
void Widget::update(const unsigned char *data)
{
    _data = data;
    updateGL();
}

