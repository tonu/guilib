#include "listwi.h"
#include "painter.h"
#include "event.h"

ListWi::ListWi(Wi *parent, Layout *layout)
    :
    Wi(parent, layout)
{
    _text.set("type\0");
}

void ListWi::paintEvent()
{
    if (_hasFocus) {

        painter->setColor(0,0,255);

        painter->drawLine(1, 1, _rect._w-1, 1);
        painter->drawLine(1, 1, 1, _rect._h-1);
        painter->drawLine(_rect._w-1, _rect._h-1, _rect._w-1, 1);
        painter->drawLine(_rect._w-1, _rect._h-1,  1, _rect._h-1);
    }
    painter->setColor(0,255,0);

    painter->drawLine(0, 0, _rect._w, 0);
    painter->drawLine(0, 0, 0, _rect._h);
    painter->drawLine(_rect._w, _rect._h, _rect._w, 0);
    painter->drawLine(_rect._w, _rect._h,  0, _rect._h);

    painter->setColor(0,0,0);
    painter->drawText(5,10, _text);

    if (_hasFocus) {
        INT8U txtX = (_text.count()-1)*6 + 5 ;
        INT8U txtY = 10 + 5;

        painter->setColor(255,0,0);
        painter->fillRect(txtX, txtY, 5,2);
    }
}

void ListWi::keyPressEvent(KeyEventData *dat)
{
    if (dat)
    {
        if (dat->_code == KeyEventData::KeyCodeEnter) // return
        {
            return;
        }
        else if (dat->_code == KeyEventData::KeyCodeBackspace) // bs
        {
            _text.removeLast();
        }
        else
        {

            Str s("...");
            s[0] = 0x30 + (INT8U)dat->_code/100;
            s[1] = 0x30 + ((INT8U)dat->_code/10) % 10;
            s[2] = 0x30 + ((INT8U)dat->_code) % 10;
            ISIG("test", s);


            Str c(".");
            c[0] = dat->_code;
            _text.append(c);

        }
        update();
    }
}

void ListWi::keyReleaseEvent(KeyEventData *dat)
{
    //    if (dat)
    //        _text.set("release %1");//, dat->_code);

    //   update();
}

void ListWi::setText(const Str &str)
{
    _text = Str() + "kp:" + str;
    update();
}
