#ifndef OBJ_H
#define OBJ_H

#include "util.h"
#include "event.h"

// to add a new signal slot type, add code to points [1] to [4]

#define CALL_MEMBER_FN(object,ptrToMember)  ((object)->*(ptrToMember))

#define ISLOT(obj, className, methodName)   new Slot<className>(obj, &className::methodName)
#define ISIG(name, args...) Obj::notify(this, name, ## args);

class Obj;
class Slo;
class Binding
{
public:
    Binding(Obj *sender, const Str & signal, Slo *slot)
    {
        _sender = sender;
        _signal = signal; // will be copied
        _slot   = slot;
    }

    Str _signal;
    Obj *_sender;
    Slo *_slot;
};



class Slo {
public:
    virtual void set() = 0;
    virtual void set(int i) = 0;
    virtual void set(const Str &str) = 0;   // [1]
};

template <class T>
        class Slot : public Slo
{
public:
    enum Type {
        TypeVoid,
        Type1Int,
        Type1Str
    };

    typedef  void (T::*MemFnVoid)();
    typedef  void (T::*MemFn1Int)(int);
    typedef  void (T::*MemFn1Str)(const Str &); // [2]

    Slot(T *obj,  MemFnVoid mem) {
        _obj = obj;
        _mem = mem;
        _type = TypeVoid;
    }
    Slot(T *obj,  MemFn1Int mem) {
        _obj = obj;
        _mem = (MemFnVoid)mem;
        _type = Type1Int;
    }
    Slot(T *obj,  MemFn1Str mem) {
        _obj = obj;
        _mem = (MemFnVoid)mem;
        _type = Type1Str;
    }

    virtual void set() {
        if (_type == TypeVoid)
            CALL_MEMBER_FN(_obj, (MemFnVoid)_mem)();
    //    else
            //qDebug() << "call args mismatch";
    }
    virtual void set(int i) {
        if (_type == Type1Int)
            CALL_MEMBER_FN(_obj, (MemFn1Int)_mem)(i);
      //  else
       //     qDebug() << "call args mismatch";
    }
    virtual void set(const Str &str) {        // [3]
        if (_type == Type1Str)
            CALL_MEMBER_FN(_obj, (MemFn1Str)_mem)(str);
       // else
       //     qDebug() << "call args mismatch";
    }

    Type _type;
    T *_obj;
    MemFnVoid _mem;
};





class Obj {

public:

    /*
    void handleInvoke(Event *ev)
    {
        InvokeEventData *dat = ev._data;
        if (dat){
            dat->_slot->set(dat->_intArg);
        }
    } */

    virtual bool handleEvent(Event *ev); // handle all default way. If you subclass, call this at the end for all default handling

    void connect(const Str & signal, Slo *slot)
    {
        Obj::conn(this, signal, slot);
    }

    static void conn(Obj *sender, const Str & signal, Slo *slot)
    {
        Binding b(sender, signal, slot);

        _list.append(b);
    }

    static void notify(Obj *sender, const Str &signal)
    {
        const Binding * b = findBinding(sender, signal);

        if (b != 0) b->_slot->set();
        /*
         new Event(EventInvoke, _obj,  new InvokeEventData(b, args?)); // implement this to get async communication
         */
    }
    static void notify(Obj *sender, const Str &signal, int val)
    {
        const Binding * b = findBinding(sender, signal);

        if (b != 0) b->_slot->set(val);
    }
    static void notify(Obj *sender, const Str &signal, const Str &val)    // [4]
    {
        const Binding * b = findBinding(sender, signal);

        if (b != 0) b->_slot->set(val);
    }

    static const Binding * findBinding(Obj *sender, const Str &signal)
    {
        List<Binding>::ConstIterator i(_list.constIterator());
        while (i.isValid())
        {
            const Binding & b = *i;
            i++;

            if (b._sender == sender &&
                b._signal == signal)
            {
                return &b;
            }
        }

        return 0;
    }

    static List<Binding> _list;
};



#endif // OBJ_H
