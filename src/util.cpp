#include "util.h"


Str operator +(const Str& lhs, const Str& rhs)
{
    Str s(rhs._count + lhs._count);

    s.append(lhs);
    s.append(rhs);

    return s;
}

